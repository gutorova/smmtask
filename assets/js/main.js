$(document).ready(function () {




  $(".js-password").on('click', function(event) {
    let input = $(this).prev();
    input.attr('type', input.attr('type') === 'text' ? 'password' : 'text');
    if (input.attr('type') === 'text') {
      $(this).addClass('show-pass');
    } else {
      $(this).removeClass('show-pass');
    }
  });



  $(".list-group  a[href^='#']").on('click', function(e) {

    // prevent default anchor click behavior
    e.preventDefault();

    // animate
    let scroll_top = $(this.hash).offset().top - $(".header").height() - 62;
    if ($(window).width() < 768) {
      scroll_top += 40;
    }
    $('html, body').animate({
      scrollTop: scroll_top
    }, 100, function(){
      // when done, add hash to url
      // (default click behaviour)
      window.location.hash = this.hash;
    });

  });



  $('.js-btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
      if(type == 'minus') {

        if(currentVal > input.attr('min')) {
          input.val(currentVal - 1).change();
        }
        if(parseInt(input.val()) == input.attr('min')) {
          $(this).attr('disabled', true);
        }

      } else if(type == 'plus') {

        if(currentVal < input.attr('max')) {
          input.val(currentVal + 1).change();
        }
        if(parseInt(input.val()) == input.attr('max')) {
          $(this).attr('disabled', true);
        }

      }
    } else {
      input.val(0);
    }
  });



  $('.js-input-number').focusin(function(){
    $(this).data('oldValue', $(this).val());
  })
  .change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    var name = $(this).attr('name');
    if(valueCurrent >= minValue) {
      $(".js-btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
      alert('Sorry, the minimum value was reached');
      $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
      $(".js-btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
      alert('Sorry, the maximum value was reached');
      $(this).val($(this).data('oldValue'));
    }
  });



  //промокод
  $("#checkpromo").change(function(){
    if ($("#checkpromo").is(":checked")){
      $("#promo").css('display', 'flex');
    }
    else{
      $("#promo").hide();
    }

  });



  $('.js-slider').flickity({
      // options
      cellAlign: 'center',
      contain: true,
      groupCells: true,
      pageDots: false,
      watchCSS: true
  });



  //пополнение баланса
  $(".js-add-balance").click(function () {
    $(this).parent().find('.js-add-balance').removeClass('active');
    $(this).addClass('active');

    $(this).parent().parent().find('.js-balance-input').val( $(this).data('value') );
  })

  $(function() {
    $(document).on("change keyup input click", ".js-balance-input, .js-input-number", function() {
      if(this.value.match(/[^0-9]/g)){
        this.value = this.value.replace(/[^0-9]/g, "");
      }
    });
  });



  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
  })

  $('.js-copy-link').click(function(){
    let link_input = $(this).parent().find('.js-copy-link-input');
    link_input.select();
    /* Copy the text inside the text field */
    document.execCommand("copy");

    // tooltip
    bootstrap.Tooltip.getInstance($(this)).hide();
    let tooltip = new bootstrap.Tooltip(link_input, {
      title: 'Текст ссылки скопирован',
      trigger: 'manual'
    });
    tooltip.show();
    window.setTimeout(function(){
      tooltip.hide();
    }, 1000);

    return false;
  });

  $('.js-copy-password').click(function(){
    let password_input = $(this).parent().parent().find('.js-copy-password-input');
    password_input.select();
    /* Copy the text inside the text field */
    document.execCommand("copy");

    // tooltip
    bootstrap.Tooltip.getInstance($(this)).hide();
    let tooltip = new bootstrap.Tooltip(password_input, {
      title: 'Пароль скопирован',
      trigger: 'manual'
    });
    tooltip.show();
    window.setTimeout(function(){
      tooltip.hide();
    }, 1000);

    return false;
  });
  jQuery.fn.selectText = function(){
    var doc = document;
    var element = this[0];
    console.log(this, element);
    if (doc.body.createTextRange) {
      var range = document.body.createTextRange();
      range.moveToElementText(element);
      range.select();
    } else if (window.getSelection) {
      var selection = window.getSelection();
      var range = document.createRange();
      range.selectNodeContents(element);
      selection.removeAllRanges();
      selection.addRange(range);
    }
  };
  $('.js-copy-area-text').click(function(){
    let text_input = $(this).parent().find('.js-copy-text-input');
    text_input.selectText();
    /* Copy the text inside the text field */
    document.execCommand("copy");

    // tooltip
    bootstrap.Tooltip.getInstance($(this)).hide();
    let tooltip = new bootstrap.Tooltip(text_input, {
      title: 'Текст скопирован',
      trigger: 'manual'
    });
    tooltip.show();
    window.setTimeout(function(){
      tooltip.hide();
    }, 1000);

    return false;
  });

  $('.js-copy-text').click(function(){
    let text_input = $(this).parent().find('.js-copy-text-input');
    text_input.select();
    /* Copy the text inside the text field */
    document.execCommand("copy");

    // tooltip
    bootstrap.Tooltip.getInstance($(this)).hide();
    let tooltip = new bootstrap.Tooltip(text_input, {
      title: 'Текст скопирован',
      trigger: 'manual'
    });
    tooltip.show();
    window.setTimeout(function(){
      tooltip.hide();
    }, 1000);

    return false;
  });

  let header_dt = $('.header .nav-link.dropdown-toggle');
  header_dt.mouseenter(function(){
    header_dt.dropdown('hide');
    $('.nav-link.dropdown').dropdown('hide');
    $(this).dropdown('show');
  });



  // рейтинг звездочки
  $('.js-review div').click(function(){
    $(this).parent().find('div').removeClass('active');
    $(this).addClass('active');
    $(this).parent().find('input').val( $(this).data('rating') );
  });



  // табы в мобиле
  $('#orderSelect').change(function(e){
    let optionSelected = $("option:selected", this);
    $('.tab-content div').removeClass('active');
    // $('.tab-content').each(function() {
    //   if($(this).find('div').hasClass('active')){
    //     $(this).find('div').removeClass('active');
    //     console.log("cfdfgdsg");
    //   }
    // });
    $('button[data-bs-target="#'+ optionSelected.data('id') +'"]').tab('show');
  });





  // copy content to clipboard
  function copyToClipboard(jqObject) {
    let $temp = $("<input>");
    jqObject.after($temp);
    $temp.val(jqObject.text()).select();
    document.execCommand("copy");
    $temp.remove();
  }

  // copy coupon code to clipboard
  $(".js-promo-copy").on("click", function(event) {
    event.preventDefault();
    copyToClipboard($(this).parent().parent().find(".text-for-copy"));
    $(this).parent().parent().find('.promo-copy').fadeIn(400);
    setTimeout(function(){$('.promo-copy').fadeOut(400);},3000);
  });


  // закрыть в шапке скидку промокод
  let block_promo = $('.js-block-promo');
  let sticky_top = 0 ;


  if (Cookies.get('js-promo-close') === '1') {
    sticky_top = $('.header').height();
    $('.sticky-lg-top').css('top', sticky_top + 'px');
    block_promo.remove();
  } else {
    block_promo.fadeIn('fast');
    sticky_top = $('.header').height() + block_promo.outerHeight();
    $('.sticky-lg-top').css('top', sticky_top + 'px');
  }
  $(".js-promo-close").on("click", function(event) {
    event.preventDefault();
    Cookies.set('js-promo-close', '1', { expires: 30 });
    block_promo.remove();
    sticky_top = $('.header').height();
    $('.sticky-lg-top').css('top', sticky_top + 'px');
  });


  // количество колонок в меню дропдаун
  $('.js-dropdown-columns').each(function () {
    let promo_height = 0;
    if ($(".js-block-promo").length >0 ) {
      promo_height = $('.js-block-promo').height()+16;
    }
    let count = Math.ceil(
        $(this).find('.dropdown-item').length
        / Math.floor(($('body').height() - $('.header').height() - promo_height) / 60)
    );

    $(this).addClass('columns-' + count);
  });
  $('.area-height').autosize();

  var textarea = document.querySelector('textarea');
  textarea.style.height="100%";

  textarea.addEventListener('keyup', function(){
    if(this.scrollTop > 0){
      this.style.height = this.scrollHeight + "px";
    }
  });
});




